﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplitedGroupOfTeachers
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Student> students = CreateStudents(17);
            Print(students);
            Console.WriteLine();
            List<Teacher> teachers = CreateTeachers(3);
            Print(teachers);
            Console.WriteLine();
            List<Group> groups = CreateGroups("C#", teachers, students);
            Print(groups);

            Console.ReadKey();
        }
        static List<Student> CreateStudents(int count)
        {
            List<Student> list = new List<Student>(count);
            for (int i = 0; i < count; i++)
            {
                list.Add(new Student(
                    $"A{i + 1}",
                    $"A{i + 1}yan", $"A{i + 1}@gmail.com",
                    i + 1));
            }
            return list;
        }
        static void Print(List<Student> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                Console.WriteLine($"{list[i].Name}, {list[i].Surnmae}, {list[i].Email}, {list[i].Age}");
            }
        }
        static List<Teacher> CreateTeachers(int count)
        {
            List<Teacher> teachers = new List<Teacher>(count);
            for (int i = 0; i < count; i++)
            {
                teachers.Add(new Teacher($"T{i + 1}"));
            }
            return teachers;
        }
        static void Print(List<Teacher> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                Console.WriteLine($"{list[i].Name}");
            }
        }
        static List<Group> CreateGroups(string groupName, List<Teacher> teachers, List<Student> students)
        {
            int groupCount = teachers.Count;
            List<Group> groups = new List<Group>(groupCount);
            foreach (Teacher teacher in teachers)
            {
                Group group = new Group
                {
                    Name = groupName,
                    Teacher = teacher,
                    Students = MoveGroup(students, students.Count/groupCount)
                };
                groups.Add(group);
                groupCount--;
            }
            return groups;
        }
        static List<Student> MoveGroup(List<Student> list, int groupCount)
        {
            List<Student> studentGroup = new List<Student>(groupCount);
            for (int i = 0; i < groupCount; i++)
            {
                studentGroup.Add(list[0]);
                list.RemoveAt(0);
            }
            return studentGroup;
        }
        static void Print(List<Group> group)
        {
            for (int i = 0; i < group.Count; i++)
            {
                Console.WriteLine();
                Console.WriteLine(group[i].Name);
                Console.WriteLine(group[i].Teacher.Name);
                Print(group[i].Students);
                Console.WriteLine();
            }
        }
    }
}
