﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplitedGroupOfTeachers
{
    class Teacher
    {
        public Teacher(string name)
        {
            Name = name;
        }
        public string Name { get; set; }
        public List<Student> List { get; set; }
    }
}
