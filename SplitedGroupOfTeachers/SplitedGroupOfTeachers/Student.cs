﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplitedGroupOfTeachers
{
    class Student
    {
        public Student(string name, string surnmae, string email, int age)
        {
            Name = name;
            Surnmae = surnmae;
            Email = email;
            Age = age;
        }
        public string Name { get; set; }
        public string Surnmae { get; set; }
        public string Email { get; set; }
        public int Age { get; set; }
    }
}
